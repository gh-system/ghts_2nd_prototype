// kr 패키지는 ghts의 한글 버전입니다.
/*******************************************************************************

GHTS : GH 프로그램 매매 시스템.

'프로그램 매매'를 하는 소프트웨어를 개발하기 위한 '라이브러리'.

'라이브러리'라는 것은 그 자체로 완성된 시스템이 '아니라',
특정 목적의 소프트웨어를 개발할 때 유용한 (혹은 유용할 수도 있는) 기능을 
가진 소스코드를 모아놓은 것을 의미함.

매매전략과 위험관리 원칙은 각자 스스로 개발해야 함.

시스템 트레이딩(System Trading : 흔히 '프로그램 매매'로 칭해짐)에 
관심있는 사람들에게 유용할 (수도 있을) 것 같아서 소스코드를 공개함.

이 소프트웨어는 MIT 라이센스를 따름.
저작권자 및 개발자들은 이 소프트웨어에 대한 그 어떠한 보증도 하지 않으며,
이 소프트웨어를 사용하면서 발생하는 그 어떠한 손실 및 손상에 대해서도 책임지지 않음.

저작권에 대한 자세한 사항은 'LICENSE' 파일을 참고할 것.

---------------------------------------------------------------------

GHTS : GH Trading System

A software library for automatic trading system.

Library means that this is NOT a complete system,
but a collection of source code for (hopefully) useful 
developing a complete system.

You should develop 'buy&sell strategy' 
and risk management principle' by yourself.

Licensed under the term of MIT License.
Refer to 'LICENSE' file, for the licensing detail.

---------------------------------------------------------------------

The MIT License

Copyright (c) 2014 UnHa Kim (김운하)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

---------------------------------------------------------------------

MIT 라이선스

Copyright (c) 2014 김운하 (UnHa Kim)

 
이 소프트웨어의 복제본과 관련된 문서화 파일(“소프트웨어”)을 획득하는 사람은 누구라도 소프트웨어를 
별다른 제한 없이 무상으로 사용할 수 있는 권한을 부여 받는다. 
여기에는 소프트웨어의 복제본을 무제한으로 사용, 복제, 수정, 병합, 공표, 배포, 서브라이선스 설정 및 
판매할 수 있는 권리와 이상의 행위를 소프트웨어를 제공받은 다른 수취인들에게 허용할 수 있는 권리가 포함되며, 
다음과 같은 조건을 충족시키는 것을 전제로 한다.

위와 같은 저작권 안내 문구와 본 허용 문구가 소프트웨어의 모든 복제본 및 중요 부분에 포함되어야 한다.

이 소프트웨어는 상품성, 특정 목적 적합성, 그리고 비침해에 대한 보증을 포함한 어떠한 형태의 보증도 
명시적이나 묵시적으로 설정되지 않은 “있는 그대로의” 상태로 제공된다.
소프트웨어를 개발한 프로그래머나 저작권자는 어떠한 경우에도 소프트웨어나 소프트웨어의 사용 등의 행위와 
관련하여 일어나는 어떤 요구사항이나 손해 및 기타 책임에 대해 
계약상, 불법행위 또는 기타 이유로 인한 책임을 지지 않는다. 

***************************************************************************/
package kr
